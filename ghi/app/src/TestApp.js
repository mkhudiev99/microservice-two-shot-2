import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import React from "react";
// import DeleteHat from './DeleteHat';

class TestApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hats: [],
        };
        this.fetchHats = this.fetchHats.bind(this);
        this.deleteHat = this.deleteHat.bind(this);
    }
    async fetchHats() {
        const response = await fetch("http://localhost:8090/api/hats/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ hats: data.hats });
        }

    }
    async deleteHat(id) {
        const hatUrl = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
            method: "delete",
            // body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const alert = document.getElementById("success-message");
            alert.classList.remove("d-none");
            // const data = await response.json();
            this.fetchHats();
        }
    }

    componentDidMount() {
        this.fetchHats();
    }

    render() {
        return (
            <BrowserRouter>
                <Nav />
                <div className="container">
                    <Routes>
                        <Route path="/" element={<MainPage />} />
                        <Route path="hats">
                            <Route path="" element={<HatsList hats={this.state.hats} deleteHat={this.deleteHat} />} />
                            <Route path="new" element={<HatsForm fetchHats={this.fetchHats} />} />
                        </Route>
                    </Routes>
                </div>
            </BrowserRouter>
        )
    }
}

export default TestApp;
