import React from 'react';
// import loadHats from './index.js'

class DeleteHat extends React.Component {

    constructor(props) {
        super(props)
        this.state = { hat: '', hats: [] };
        this.handleHatChange = this.handleHatchange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.hats;

        const selectTag = document.getElementById('hat');
        const optionValue = selectTag.options[selectTag.selectedIndex].value;
        const hatUrl = `http://localhost:8090/api/hats/${optionValue}/`
        const fetchConfig = {
            method: "delete",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            // loadHats();
            const alert = document.getElementById("success-message");
            alert.classList.remove("d-none");
            const cleared = {
                hat: '',
            };
            this.setState(cleared);
        }
    }

    handleHatchange(event) {
        const value = event.target.value;
        this.setState({ hat: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ hats: data.hats });
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Delete Hat</h1>
                        <form onSubmit={this.handleSubmit} id="delete-hat-form">
                            <div className="mb-3">
                                <select value={this.state.hat} onChange={this.handleHatChange} required name="hat" id="hat" className="form-select">
                                    <option value="">Choose a Hat</option>
                                    {this.state.hats.map(hat => {
                                        return (
                                            <option key={hat.id} value={hat.id}>
                                                {hat.color.toUpperCase()} {hat.style.toUpperCase()} {hat.fabric.toUpperCase()} HAT
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Delete</button>
                        </form>
                        <div className="alert alert-success d-none mb-0" id="success-message">
                            It's always good to switch it up every now and then. Hat deleted!
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}
export default DeleteHat;
