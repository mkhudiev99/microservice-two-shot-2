import React from 'react';
// import loadHats from './index.js'

class HatsForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            style: '',
            color: '',
            fabric: '',
            pictureUrl: '',
            location: '',
            locations: []
        };
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.locations;


        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            this.props.fetchHats()
            // loadHats();
            const alert = document.getElementById("success-message");
            alert.classList.remove("d-none");
            const cleared = {
                style: '',
                color: '',
                fabric: '',
                pictureUrl: '',
                location: ''
            };
            this.setState(cleared);
        }
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value })
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value })
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    render() {
        return (

            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a New Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style"
                                    className="form-control" />
                                <label htmlFor="style">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color"
                                    className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric"
                                    className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.pictureUrl} onChange={this.handlePictureUrlChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url"
                                    className="form-control" />
                                <label htmlFor="pictureUrl">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.location} onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.id}>
                                                {location.closet_name} Section {location.section_number} Shelf {location.shelf_number}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div className="alert alert-success d-none mb-0" id="success-message">
                            Hat added to inventory!
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default HatsForm;
