import React from "react";
// import DeleteHat from "./DeleteHat";
// import TestApp from "./TestApp";


function HatsList(props) {

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.style}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td><button onClick={() => props.deleteHat(hat.id)}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="alert alert-success d-none mb-0" id="success-message">
                It's always good to switch it up every now and then. Hat deleted!
            </div>
        </>
    )
}

export default HatsList;

// class HatsList extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             dataRow: [],
//         };
//     }


//     async componentDidMount() {
//         const url = 'http://localhost:8090/api/hats/';
//         try {
//             const response = await fetch(url);
//             if (response.ok) {
//                 const data = await response.json();
//                 const requests = [];
//                 for (let hat of data.hats) {
//                     const detailUrl = `http://localhost:8090/api/hats/{hat.id}/`;
//                     requests.push(fetch(detailUrl));
//                     const responses = await Promise.all(requests);
//                 }
//             }
//         }
//     }
// }
