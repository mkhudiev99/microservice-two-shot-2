from django.urls import path

from .api_views import(
    api_list_hat,
    api_show_hat,
)

urlpatterns = [
    path("hats/", api_list_hat, name="api_create_hat"),
    path("locations/<int:location_vo_id>/hats/", api_list_hat, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
]
