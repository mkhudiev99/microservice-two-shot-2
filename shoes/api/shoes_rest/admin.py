from django.contrib import admin
from .models import BinVO, Shoe
# Register your models here.


@admin.register(BinVO)
class BinVO(admin.ModelAdmin):
    pass


@admin.register(Shoe)
class Shoe(admin.ModelAdmin):
    pass