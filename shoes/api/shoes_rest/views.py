from django.shortcuts import render
from common.json import ModelEncoder
from .models import BinVO, Shoe
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class BinEncoder(ModelEncoder):
    model = BinVO
    properties =[
        "closet_name",
        "bin_size",
        "bin_number",
        "id",
    ]
    
    
class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
    ]
    encoders = {
        "bin": BinEncoder(),
    }
    
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

    
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    
    encoders = {
        "bin": BinEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoe(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else: 
            shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder = ShoeEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            
            bin = BinVO.objects.get(id = content["bin"])
            content["bin"] = bin
            print("JHGJGHGHJHJGJHGGHJKJKGGJK", content)
    
        except BinVO.DoesNotExist:
            return JsonResponse({"message:" "Bin does not exist"}, encoder=BinEncoder, status=400, safe=False)
        
        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder= ShoeEncoder, safe=False)
    
    
    
    
    
    
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0 })
    elif request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe, encoder= ShoeDetailEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
        
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    
